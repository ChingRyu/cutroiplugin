﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

using Pluto;

namespace Plugin
{
    [PluginType(PluginCategory.Filter)]
    public class RestoreROI : ICommonPlugin, IMenuEx
    {
        #region フィールド
        DataManager dataManager = null;
        IPluto iPluto = null;
        #endregion

        #region IPlugin メンバ
        string IPlugin.Author { get { return ("PLUTO Development Team"); } }

        string IPlugin.Text { get { return ("RestoreROI"); } }

        string IPlugin.Comment { get { return ("Restore ROI data back to original data."); } }

        bool IPlugin.Initialize(DataManager data, IPluto pluto)
        {
            // DataManager および IPluto を取得する．
            dataManager = data;
            iPluto = pluto;

            return (true);
        }
        #endregion

        void Convert(int id, int dim_x, int dim_y, int dim_z, out int x, out int y, out int z)
        {
            x = id % dim_x;
            y = (id / dim_x) % dim_y;
            z = id / (dim_x * dim_y);
        }

        #region ICommonPlugin メンバ
        object ICommonPlugin.Run(params object[] args)
        {
            // DataManager および IPluto の取得に失敗している．または，画像が選択されていない．
            if (dataManager == null || iPluto == null || dataManager.Active == null || dataManager.Count < 3)
            {
                return (null);
            }


            Mist.MistArray origin = dataManager.Active;

            Mist.MistArray mask = null;
            Mist.MistArray roi = null;

            int index = dataManager.IndexOf(dataManager.Active);
            if (dataManager.Count <= index + 2) //make sure globalDATA lays at the bottom, mask at center and ROI at top.
                return null;
            
            mask = dataManager[index + 1];
            roi = dataManager[index + 2];

            ArrayList points_x = new ArrayList();
            ArrayList points_y = new ArrayList();
            ArrayList points_z = new ArrayList();

            for (int i = 0; i < mask.Size; ++i)
            {
                if (mask[i] > 0)
                {
                    int x = 0;
                    int y = 0;
                    int z = 0;
                    Convert(i, mask.Size1, mask.Size2, mask.Size3, out x, out y, out z);
                    points_x.Add(x);
                    points_y.Add(y);
                    points_z.Add(z);
                }
            }

            points_x.Sort();
            points_y.Sort();
            points_z.Sort();
            int length_x = (int)points_x[points_x.Count - 1] - (int)(int)points_x[0];
            int length_y = (int)points_y[points_y.Count - 1] - (int)(int)points_y[0];
            int length_z = (int)points_z[points_z.Count - 1] - (int)(int)points_z[0];
            int center_x = ((int)points_x[points_x.Count - 1] + (int)(int)points_x[0]) / 2;
            int center_y = ((int)points_y[points_y.Count - 1] + (int)(int)points_y[0]) / 2;
            int center_z = ((int)points_z[points_z.Count - 1] + (int)(int)points_z[0]) / 2;

//             int mk_i = System.Math.Max((center_x - length / 2), 0);
//             int mk_j = System.Math.Max((center_y - length / 2), 0);
//             int mk_k = System.Math.Max((center_z - length / 2), 0);

            for (int i = 0; i < length_x; ++i)
            {
                for (int j = 0; j < length_y; ++j)
                {
                    for (int k = 0; k < length_z; ++k)
                    {

                        origin[i + (int)points_x[0], j + (int)points_y[0], k + (int)points_z[0]] = roi[i, j, k];
                    }
                }
            }

            return (null);
        }
        #endregion

        #region IMenuEx メンバ
        bool IMenuEx.Visible
        {
            get
            {
                // DataManager が設定されている場合は表示，されていない場合は非表示とする．
                return (dataManager != null);
            }
        }

        bool IMenuEx.Enabled
        {
            get
            {
                if (dataManager == null)
                {
                    // DataManager が設定されていない場合は無効とする．
                    return (false);
                }
                else
                {
                    // Active なデータがある場合は有効，ない場合は無効とする．
                    return (dataManager.Active != null);
                }
            }
        }

        string IMenuEx.Category
        {
            get
            {
                return ("Utility");
            }
        }
        #endregion
    }
}
