﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows;
using System.Runtime.InteropServices;

using Pluto;

namespace Plugin
{
    // メニューのサブカテゴリを指定する．
    [Category("Utility")]
    [PluginType(PluginCategory.Filter)]
    public partial class FilterPlugin : ICommonPlugin, IMenuEx
    {
        #region フィールド
        DataManager dataManager = null;
        IPluto iPluto = null;
        #endregion

        #region IPlugin メンバ
        string IPlugin.Author { get { return ("PLUTO Development Team"); } }

        string IPlugin.Text { get { return ("CutROI"); } }

        string IPlugin.Comment { get { return ("Cut ROI Filter Plugin"); } }

        bool IPlugin.Initialize(DataManager data, IPluto pluto)
        {
            // DataManager および IPluto を取得する．
            dataManager = data;
            iPluto = pluto;

            return (true);
        }
        #endregion

        void Convert(int id, int dim_x, int dim_y, int dim_z, out int x, out int y, out int z)
        {
            x = id % dim_x;
            y = (id / dim_x) % dim_y;
            z = id / (dim_x * dim_y);
        }

        #region ICommonPlugin メンバ
        object ICommonPlugin.Run(params object[] args)
        {
            // DataManager および IPluto の取得に失敗している．または，画像が選択されていない．
            if (dataManager == null || iPluto == null || dataManager.Active == null)
            {
                return (null);
            }

            Mist.MistArray ct = dataManager.Active;

            Mist.MistArray mask = null;
            int index = dataManager.IndexOf(dataManager.Active);
            if (dataManager.Count <= index + 1)
                return null;
            mask = dataManager[index + 1];

            ArrayList points_x = new ArrayList();
            ArrayList points_y = new ArrayList();
            ArrayList points_z = new ArrayList();

            for (int i = 0; i < mask.Size; ++i )
            {
                if(mask[i] > 0)
                {
                    int x = 0;
                    int y = 0;
                    int z = 0;
                    Convert(i, mask.Size1, mask.Size2, mask.Size3, out x, out y, out z);
                    points_x.Add(x);
                    points_y.Add(y);
                    points_z.Add(z);
                }
            }

            points_x.Sort();
            points_y.Sort();
            points_z.Sort();
            int length_x = (int)points_x[points_x.Count - 1] - (int)(int)points_x[0];
            int length_y = (int)points_y[points_y.Count - 1] - (int)(int)points_y[0];
            int length_z = (int)points_z[points_z.Count - 1] - (int)(int)points_z[0];
            int center_x = ((int)points_x[points_x.Count - 1] + (int)(int)points_x[0])/ 2;
            int center_y = ((int)points_y[points_y.Count - 1] + (int)(int)points_y[0])/ 2;
            int center_z = ((int)points_z[points_z.Count - 1] + (int)(int)points_z[0])/ 2;

//            int length = System.Math.Max(System.Math.Max(length_x, length_y), length_z);
            Mist.MistArray mk = new Mist.MistArray(length_x, length_y, length_z, ct.Reso1, ct.Reso2, ct.Reso3);
            mk.Fill(0);

//             int mk_i = System.Math.Max((center_x - length / 2), 0);
//             int mk_j = System.Math.Max((center_y - length / 2), 0);
//             int mk_k = System.Math.Max((center_z - length / 2), 0);

            for (int i = 0; i < length_x; ++i)
            {
                for (int j = 0; j < length_y; ++j )
                {
                    for (int k = 0; k < length_z; ++k)
                    {

                        mk[i, j, k] = ct[i + (int)points_x[0], j + (int)points_y[0], k + (int)points_z[0]];
                    }
                }
            }

            // 出力画像を DataManager へ追加する．
            if (dataManager.Add(mk, true, false) < 0)
            {
                // 追加に失敗したら，出力画像のリソースを開放する．
                mk.Dispose();
                return (null);
            }
            else
            {
                return (mk);
            }

        }
        #endregion

        #region IMenuEx メンバ
        bool IMenuEx.Visible
        {
            get
            {
                // DataManager が設定されている場合は表示，されていない場合は非表示とする．
                return (dataManager != null);
            }
        }

        bool IMenuEx.Enabled
        {
            get
            {
                if (dataManager == null)
                {
                    // DataManager が設定されていない場合は無効とする．
                    return (false);
                }
                else
                {
                    // Active なデータがある場合は有効，ない場合は無効とする．
                    return (dataManager.Active != null);
                }
            }
        }

        string IMenuEx.Category
        {
            get
            {
                return ("Utility");
            }
        }
        #endregion
    }
}